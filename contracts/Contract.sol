pragma solidity >=0.4.22 <0.6.0;

contract Contract {
    address public owner;

    constructor() public {
        owner = msg.sender;
    }

    function echo(string memory input) public view returns (string memory) {
        string memory output = input;
        return output;
    }
}
